package it.polito.ezgas.converter;

import java.util.List;
import java.util.ArrayList;

import it.polito.ezgas.dto.UserDto;
import it.polito.ezgas.entity.User;

public class UserConverter implements CustomConverter<User, UserDto>{

	@Override
	public UserDto convertToDto(User aUser) {
		UserDto userDto = new UserDto(
									aUser.getUserId(), 
									aUser.getUserName(), 
									aUser.getPassword(), 
									aUser.getEmail(), 
									aUser.getReputation(), 
									aUser.getAdmin()
									);
		return userDto;
	}

	@Override
	public User convertToEntity(UserDto aUserDto) {
		User user = new User(
							aUserDto.getUserName(), 
							aUserDto.getPassword(), 
							aUserDto.getEmail(), 
							aUserDto.getReputation() 
							);
		return user;
	}

	@Override
	public Iterable<UserDto> convertToDtos(Iterable<User> aListOfEntities) {
		List<UserDto> listDtos = new ArrayList<UserDto>();
		UserDto newUserDto;
		for(User user: aListOfEntities) {
			newUserDto = convertToDto(user);
			listDtos.add(newUserDto);
		}
		return null;
	}

}
