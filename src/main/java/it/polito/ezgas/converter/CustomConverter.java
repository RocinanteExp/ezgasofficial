package it.polito.ezgas.converter;

public interface CustomConverter<E, D> {
	D convertToDto(E aEntity);
	E convertToEntity(D aDto);
	Iterable<D> convertToDtos(Iterable<E> aListOfEntities);
}
