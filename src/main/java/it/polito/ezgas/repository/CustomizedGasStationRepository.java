package it.polito.ezgas.repository;

import it.polito.ezgas.entity.GasStation;
import java.util.List;

public interface CustomizedGasStationRepository {
	
	List<GasStation> findAllByGasolineType(String gasolineType); 
	List<GasStation> findAllByCarSharing(String carSharing);
	List<GasStation> findAllByGasolineTypeAndCarSharing(String gasolineType, String carSharing);
	List<GasStation> findAllByProximity(double lat, double lon);
	List<GasStation> findAllWithCoordinates(double lat, double lon, String gasolineType, String carSharing);
	
}
