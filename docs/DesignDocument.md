# Design Document 

Authors: Marco Bellavia (s280130), Gabriel Ganzer (s271961), Gianluca Morabito (s277943), Francesco Xia (s277509)

Date:    02/05/2020

Version: 1

# Contents

- [High level design](#high-level-design)
- [Low level design](#low-level-design)
- [Verification traceability matrix](#verification-traceability-matrix)
- [Verification sequence diagrams](#verification-sequence-diagrams)

# Instructions

The design must satisfy the Official Requirements document (see EZGas Official Requirements.md ). <br>
The design must comply with interfaces defined in package it.polito.ezgas.service (see folder ServicePackage ) <br>
UML diagrams **MUST** be written using plantuml notation.

# High level design 

The style selected is client - server. Clients can be smartphones, tablets, PCs.
The choice is to avoid any development client side. The clients will access the server using only a browser. 

The server has two components: the frontend, which is developed with web technologies (JavaScript, HTML, Css) and is in charge of collecting user inputs to send requests to the backend; the backend, which is developed using the Spring Framework and exposes API to the front-end.
Together, they implement a layered style: Presentation layer (front end), Application logic and data layer (back end). 
Together, they implement also an MVC pattern, with the V on the front end and the MC on the back end.



```plantuml
@startuml
package "Backend" {

}

package "Frontend" {

}


Frontend -> Backend
@enduml


```


## Front End

The Frontend component is made of: 

Views: the package contains the .html pages that are rendered on the browser and that provide the GUI to the user. 

Styles: the package contains .css style sheets that are used to render the GUI.

Controller: the package contains the JavaScript files that catch the user's inputs. Based on the user's inputs and on the status of the GUI widgets, the JavaScript controller creates REST API calls that are sent to the Java Controller implemented in the back-end.


```plantuml
@startuml
package "Frontend" {

    package "it.polito.ezgas.resources.views" {

    }


package "it.polito.ezgas.resources.controller" {

    }


package "it.polito.ezgas.resources.styles" {

    }



it.polito.ezgas.resources.styles -down-> it.polito.ezgas.resources.views

it.polito.ezgas.resources.views -right-> it.polito.ezgas.resources.controller


}
@enduml

```

## Back End

The backend  uses a MC style, combined with a layered style (application logic, data). 
The back end is implemented using the Spring framework for developing Java Entrerprise applications.

Spring was selected for its popularity and relative simplicity: persistency (M and data layer) and interactions are pre-implemented, the programmer needs only to add the specific parts.

See in the package diagram below the project structure of Spring.

For more information about the Spring design guidelines and naming conventions:  https://medium.com/the-resonant-web/spring-boot-2-0-project-structure-and-best-practices-part-2-7137bdcba7d3



```plantuml
@startuml
package "Backend" {

package "it.polito.ezgas.service"  as ps {
   interface "GasStationService"
   interface "UserService"
} 


package "it.polito.ezgas.controller" {

}

package "it.polito.ezgas.converter" {

}

package "it.polito.ezgas.dto" {

}

package "it.polito.ezgas.entity" {

}

package "it.polito.ezgas.repository" {

}

    
}
note "see folder ServicePackage" as n
n -- ps
```



The Spring framework implements the MC of the MVC pattern. The M is implemented in the packages Entity and Repository. The C is implemented in the packages Service, ServiceImpl and Controller. The packages DTO and Converter contain classes for translation services.



**Entity Package**

Each Model class should have a corresponding class in this package. Model classes contain the data that the application must handle.
The various models of the application are organised under the model package, their DTOs(data transfer objects) are present under the dto package.

In the Entity package all the Entities of the system are provided. Entities classes provide the model of the application, and represent all the data that the application must handle.




**Repository Package**

This package implements persistency for each Model class using an internal database. 

For each Entity class, a Repository class is created (in a 1:1 mapping) to allow the management of the database where the objects are stored. For Spring to be able to map the association at runtime, the Repository class associated to class "XClass" has to be exactly named "XClassRepository".

Extending class JpaRepository provides a lot of CRUD operations by inheritance. The programmer can also overload or modify them. 



**DTO package**

The DTO package contains all the DTO classes. DTO classes are used to transfer only the data that we need to share with the user interface and not the entire model object that we may have aggregated using several sub-objects and persisted in the database.

For each Entity class, a DTO class is created (in a 1:1 mapping).  For Spring the Dto class associated to class "XClass" must be called "XClassDto".  This allows Spring to find automatically the DTO class having the corresponding Entity class, and viceversa. 




**Converter Package**

The Converter Package contains all the Converter classes of the project.

For each Entity class, a Converter class is created (in a 1:1 mapping) to allow conversion from Entity class to DTO class and viceversa.

For Spring to be able to map the association at runtime, the Converter class associated to class "XClass" has to be exactly named "XClassConverter".




**Controller Package**

The controller package is in charge of handling the calls to the REST API that are generated by the user's interaction with the GUI. The Controller package contains methods in 1:1 correspondance to the REST API calls. Each Controller can be wired to a Service (related to a specific entity) and call its methods.
Services are in packages Service (interfaces of services) and ServiceImpl (classes that implement the interfaces)

The controller layer interacts with the service layer (packages Service and ServieImpl) 
 to get a job done whenever it receives a request from the view or api layer, when it does it should not have access to the model objects and should always exchange neutral DTOs.

The service layer never accepts a model as input and never ever returns one either. This is another best practice that Spring enforces to implement  a layered architecture.



**Service Package**


The service package provides interfaces, that collect the calls related to the management of a specific entity in the project.
The Java interfaces are already defined (see file ServicePackage.zip) and the low level design must comply with these interfaces.


**ServiceImpl Package**

Contains Service classes that implement the Service Interfaces in the Service package.

# Low level design

### it.polito.ezgas.entity

```plantuml
@startuml
'class diagram for "it.polito.ezgas.entity"

class User{
--private fields--
-userId : Integer
-username : String
-password : String
-email : String
-reputation : Integer
-isAdmin : Boolean
--public methods--
+getters()
+setters()
}

class GasStation{
--private fields--
-gasStationId : Integer
-gasStationName : String
-gasStationAddress : String
-lat : double
-long : double
-hasDiesel : Boolean
-hasSuper : Boolean
-hasSuperPlus : Boolean
-hasGas : Boolean
-hasMethane : Boolean
-carSharing : String
-aPriceList : PriceList
--public methods--
+getters()
+settters()
}

class PriceList{
-reportUser : Integer
-reportTimestamp : String
-reportDependability: Integer
-dieselPrice : double
-superPrice : double
-superPlusPrice : double
-gasPrice : double
-methanePrice : double
--public methods--
+getters()
+settters()
}

GasStation +-- PriceList : inner class
@enduml
```

### it.polito.ezgas.dto
```plantuml
@startuml
'class diagram for "it.polito.ezgas.dto"

class UserDto{
-userId : Integer
-username : String
-password : String
-email : String
-reputation : Integer
-isAdmin : Boolean
--public methods--
+getters()
+setters()
}

class GasStationDto{
--private fields--
-gasStationId : Integer
-gasStationName : String
-gasStationAddress : String
-lat : double
-log : double
-hasDiesel : Boolean
-hasSuper : Boolean
-hasSuperPlus : Boolean
-hasGas : Boolean
-hasMethane : Boolean
-carSharing : String
-reportUser : Integer
-reportTimestamp : String
-reportDependability: Integer
-dieselPrice : double
-superPrice : double
-superPlusPrice : double
-gasPrice : double
-methanePrice : double
--public methods--
+getters()
+setters()
}

class LoginDto{
--private fields--
-token : String
-userId : Integer
-username : String
-email : String
-reputation : Integer
-isAdmin : Boolean
--public methods--
+getters()
+setters()
}

class IdPw{
--private fields--
-username : String
-password : Integer
--public methods--
+getters()
+setters()
}
@enduml
```
### it.polito.ezgas.converter
```plantuml
@startuml
'class diagram for "it.polito.ezgas.converter"

class UserConverter{
--public static methods--
+convertToDto(User aUser) : UserDto
+convertToEntity(UserDto aUserDto) : User
+convertToDtos(Iterable<User> users) : List<UserDto>
}

class GasStationConverter{
--public static methods--
+convertToDto(GasStation aGasStation) : GasStationDto
+convertToEntity(GasStationDto aGasStationDto) : GasStation
+convertToDtos(Iterable<GasStation> gasStations) : List<GasStationDto>
}

@enduml
```

### it.polito.ezgas.controller + it.polito.ezgas.service
```plantuml
@startuml
left to right direction
package it.polito.ezgas.service{
'class diagram for “it.polito.ezgas.service”
interface UserService{
--public methods--
+getUserById(Integer userId) : UserDto
+getAllUsers() : List<UserDto>
+saveUser(UserDto userDto) : UserDto
+deleteUser(Integer userId) : Boolean
+increaseUserReputation(Integer userId) : Integer
+decreaseUserReputation(Integer userId) : Integer
+login(IdPw credentials) : LoginDto
}

interface GasStationService {
--public methods--
+getGasStationById(Integer gasStationId) : GasStationDto
+getAllGasStations() : List<GasStationDto>
+saveGasStation(GasStationDto gasStationDto) : GasStationDto
+deleteGasStation(Integer gasStationId) : Boolean
+getGasStationsByGasolineType(String gasolinetype) : List<GasStationDto>
+getGasStationByCarSharing(String carSharing) :  List<GasStationDto>
+getGasStationsWithoutCoordinates(String gasolinetype, String carsharing) :  List<GasStationDto>
+getGasStationsByProximity(double lat, double lon) : List<GasStationDto>
+getGasStationsWithCoordinates(double lat, double lon, String gasolinetype, String carsharing) :  List<GasStationDto>
+setReport(Integer gasStationId, double , double , double , double , double , Integer userId) : void
}

class UserServiceImpl{
--private fields--
-aUserRepository : UserRepository
}
class GasStationServiceImpl{
--private fields--
-aGasStationRepository : GasStationRepository
--static methods--
+updateTrustLevelPriceListDaily()
}
UserServiceImpl .|> UserService : <<implement>>
GasStationServiceImpl .|> GasStationService : <<implement>>
}

'class diagram for it.polito.ezgas.controller
package it.polito.ezgas.controller{

class UserController{
--private fields--
-aUserService : UserService
--public methods--
+getUserById(Integer userId) : UserDto
+List<UserDto> getAllUsers() : List<UserDto>
+saveUser(UserDto userDto) : UserDto
+deleteUser(Integer userId) : Boolean
+increaseUserReputation(Integer userId) : Integer
+decreaseUserReputation(Integer userId) : Integer
+login(IdPw credentials) : LoginDto
}

class GasStationController{
--private fields--
-aGasStationService: GasStationService
--public methods--
+getGasStationById(Integer gasStationId) : GasStationDto
+getAllGasStations() : List<GasStationDto>
+saveGasStation(GasStationDto gasStationDto) : GasStationDto
+deleteGasStation(Integer gasStationId) : void
+getGasStationsByGasolineType(String gasolinetype) : List<GasStationDto>
+getGasStationByCarSharing(String carSharing) :  List<GasStationDto> 
+getGasStationsByProximity(double lat, double lon) : List<GasStationDto>
+getGasStationsWithCoordinates(double lat, double lon, String gasolinetype, String carsharing) :  List<GasStationDto>
+setGasStationReport(Integer gasStationId, double , double , double , double , double , Integer userId) : void
+getGasStationsWithoutCoordinates(String gasolinetype, String carsharing) :  List<GasStationDto> 
}

note bottom of GasStationController: the methods getGasStationByCarSharing() and getGasStationsWithoutCoordinates() are missing from the source code 

}

'DEPENDECIES
it.polito.ezgas.service <.. it.polito.ezgas.controller : <<use>>
@enduml
```

### it.polito.ezgas.repository
```plantuml
@startuml
'class diagram for “it.polito.ezgas.repository”
interface UserRepository{
+findByUsername
}

interface GasStationRepository{
}

interface CustomizedGasStationRepository {
+findAllByGasolineType(String gasolineType) : List<GasStation>
+findAllByCarSharing(String carSharing) :  List<GasStation>
+findAllByGasolineTypeAndCarSharing(String gasolineType, String carSharing) :  List<GasStation>
+findAllByProximity(double lat, double lon) : List<GasStation>
+findAllWithCoordinates(double lat, double lon, String gasolineType, String carSharing) :  List<GasStation>
}

class GasStationRepositoryImpl{
}

interface CrudRepository<T,ID>{
+count() : long 
+delete(T entity) : void
+deleteAll(T entity) : void
+deleteAll(Iterable<T> entities): void
+deleteById(ID id) : void
+existsById(ID id) : Boolean
+findAll() : Iterable<T>
+findAllById(Iterable<ID> ids) : Iterable<T>
+findById(ID id) : Optional<T>
+save(T entity) : T
+saveAll(Iterable<T> entities) : Iterable<T>
}


GasStationRepository --|> CrudRepository : <<extend>>
GasStationRepository --|> CustomizedGasStationRepository : <<extend>>
GasStationRepositoryImpl ..|> CustomizedGasStationRepository : <<implement>>
UserRepository --|> CrudRepository : <<extend>>
note top of UserRepository :  T = User, ID = Integer
note top of GasStationRepository : T = GasStation, ID = Integer
@enduml
```

```plantuml
@startuml
left to right direction
scale 850 width
package it.polito.ezgas.controller {
class UserController
class GasStationController
}

package it.polito.ezgas.converter {
class UserConverter
class GasStationConverter
}

package it.polito.ezgas.dto{
class UserDto
class GasStationDto
class IdPw
class LoginDto
}

package it.polito.ezgas.entity {
class User
class GasStation
}

package it.polito.ezgas.repository {
interface CrudRepository
interface UserRepository
interface CustomizedGasStationRepository
class GasStationRepositoryImpl
interface GasStationRepository
UserRepository --|> CrudRepository : <<extend>>
GasStationRepository --|> CrudRepository: <<extend>>
GasStationRepository --|> CustomizedGasStationRepository: <<extend>>
GasStationRepositoryImpl ..|> CustomizedGasStationRepository: <<implement>>
}

package it.polito.ezgas.service {
interface UserService
interface GasStationService
class UserServiceImpl
class GasStationServiceImpl
UserServiceImpl .|> UserService : implements
GasStationServiceImpl.|> GasStationService: implements
}
t.polito.ezgas.controller .right.> it.polito.ezgas.service
it.polito.ezgas.service .down..> it.polito.ezgas.repository
it.polito.ezgas.service .down..> it.polito.ezgas.converter
it.polito.ezgas.converter .left.> it.polito.ezgas.entity
it.polito.ezgas.converter .left.> it.polito.ezgas.dto
it.polito.ezgas.repository .down.> it.polito.ezgas.entity
@enduml
```

# Verification traceability matrix

|ID FR | UserController  | GasStationController | UserServiceImpl | GasStationControllerImpl | UserRepository | GasStationRepository| UserConverter | GasStationConverter | User | GasStation |UserDto |GasStationDto | LoginDto | IdPw |
| :--  |:--:| :--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|
|FR1   |x | |x | |x | |x | |x | |x | |x |x |
|FR1.1 |x | |x | |x | |x | |x | |x | |x |x |
|FR1.2 |x | |x | |x | |x | |x | |x | |x |x |
|FR1.3 |x | |x | |x | |x | |x | |x | |x |x |
|FR1.4 |x | |x | |x | |x | |x | |x | |x |x |
|FR2 || || || ||| ||| | ||
|FR3   | |x | |x | |x | |x | |x | |x |x |x |
|FR3.1 | |x | |x | |x | |x | |x | |x |x |x |
|FR3.2 | |x | |x | |x | |x | |x | |x |x |x |
|FR3.3 | |x | |x | |x | |x | |x | |x |x |x |
|FR4   | |x | |x | |x | |x | |x | |x |  |  |
|FR4.1 | |x | |x | |x | |x | |x | |x |  |  |
|FR4.2 | |x | |x | |x | |x | |x | |x |  |  |
|FR4.3 | |x | |x | |x | |x | |x | |x |  |  |
|FR4.4 | |x | |x | |x | |x | |x | |x |  |  |
|FR4.5 | |x | |x | |x | |x | |x | |x |  |  |
|FR5   |x|x |x |x |x |x |x |x |x |x |x |x |x |x |
|FR5.1 | |x | |x | |x | |x | |x | |x |x |x |
|FR5.2 | |x | |x | |x | |x | |x | |x |x |x |
|FR5.3 |x | |x | |x | |x | |x | |x | |x |x |

# Verification sequence diagrams 
![SD1](https://raw.githubusercontent.com/MarcoBll/SoftEng__1/master/New_SD1.PNG)

![SD4](https://raw.githubusercontent.com/MarcoBll/SoftEng__1/master/New_SD4.PNG)

![SD7](https://raw.githubusercontent.com/MarcoBll/SoftEng__1/master/New_SD7.PNG)

![SD10](https://raw.githubusercontent.com/MarcoBll/SoftEng__1/master/New_SD10.PNG)






